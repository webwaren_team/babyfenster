
- Im data.xml können die Einstellungen zum Banner gesetzt werden.

- Ans Flash-Objekt (im sample.html) können folgende Parameter (FlashVars) übergeben werden:
lang (de, en, fr)
setupfile (data.xml)


Um neue Schriften einzubetten musst du nur einen Schritt in Flash vornehmen:
- Neue Schrift hinzufügen (in der Library)
  - Export for Actionscript + Export in Frame 1
  - Identifier für die Schrift setzen


- Um die Schrift zu verwenden, kannst du den Namen, welchen du unter "Identifier" eingegeben hast, im XML eingeben.


pazaaa@gmail.com
www.patrickzahnd.ch